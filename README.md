# Django University Management System
## Project Overview
This Django project emulates a University Management System similar to Studomat or Moodle. The system has three user roles: Admin, Professor, and Student. It uses Django and SQLite3 to facilitate the user management and course allocation processes. The application is initiated from a login page.

## User Roles
### 1. Admin
 An admin has the authority to manage all the students, professors, and courses. Admin can add, edit, or remove students, professors, and courses, assign or unassign a professor to a course, enroll or unenroll a student from a course, and see which professors are assigned to a course and which students are enrolled in a course.

### 2. Professor
 A professor can only view the courses they've been assigned to. For each course, the professor can see the enrolled students and can pass or fail a student by granting or revoking a course signature.

### 3. Student 
A student can see all the courses they've been enrolled in, all the courses they've passed or failed, and can enroll or unenroll themselves from a course.

## Installation & Setup
1. Clone the project repository to your local machine.
2. Navigate to the project directory.
3. Install the required dependencies using pip: ```pip install -r requirements.txt```
4. Run the following command to set up the database: ```python manage.py migrate```
5. Load the provided fixture file: ```python manage.py loaddata predmeti.json```

## Creating a Superuser
To create a superuser or admin who can access the admin page, follow these steps:
1. Run the following command in the terminal: ```python manage.py createsuperuser```
2. You'll be asked to provide a username, email, and password for the superuser. Once you've entered these details, your superuser will be created and you can use these credentials to log in to the admin page.

## Running the Server
1. To start the server, use the following command: ```python manage.py runserver```
2. Open your web browser and visit ```'http://127.0.0.1:8000/'``` to see the application running.

## Contributing
This project is open for contributions. Please fork the repository and create a pull request for any features, enhancements, or bug fixes.
