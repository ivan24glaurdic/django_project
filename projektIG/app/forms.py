from django import forms
from .models import *
from django.contrib.auth.hashers import make_password

class Predmetiforma(forms.ModelForm):
    class Meta:
        model=Predmeti
        fields=["name","kod","program","ects","sem_red","sem_izv","izborni"]

class Korsinik(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model=Korisnik
        fields=["username","first_name","last_name","email","password","role","status"]
    def clean_password(self):
        password=make_password(self.cleaned_data.get("password"))
        return password

