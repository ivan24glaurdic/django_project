from django.shortcuts import render,redirect
from django.http import HttpResponseNotAllowed,HttpResponse
from .models import Korisnik,UpisnilistStudent,Predmeti,UpisnilistProfesora
from app import forms
# Create your views here.

def preusmjeri(request):
    uloga=request.user.role
    if(uloga == "prof"):
       return redirect("Profesor",request.user.id)
    elif(uloga == "stu"):
        return redirect("Student",request.user.id)
    elif(uloga == "admin"):
        return redirect("admin1")
    else:
        return HttpResponseNotAllowed()


def admin(request):
    predmeti = Predmeti.objects.all()
    studenti = Korisnik.objects.filter(role="stu")
    profesori= Korisnik.objects.filter(role="prof")
    return render(request,"admin.html",{"predmeti":predmeti,"studenti": studenti , "profesori": profesori})

def addpredmet(request):
    if(request.method=="GET"):
        form=forms.Predmetiforma()
    elif(request.method=="POST"):
        form=forms.Predmetiforma(request.POST)
        if(form.is_valid()):
            form.save()
            return redirect("admin1")
    return render(request,"addpred.html",{"form": form})


def addKorsinika(request):
    if(request.method=="GET"):
        form=forms.Korsinik()
    elif(request.method=="POST"):
        form=forms.Korsinik(request.POST)
        if(form.is_valid()):
            form.save()
            return redirect("admin1")
    return render(request,"addKorisnik.html",{"form": form})

def updatePredmet(request,predmet_id):
    predmet=Predmeti.objects.get(pk=predmet_id)
    if(request.method=="GET"):
        form=forms.Predmetiforma(instance=predmet)
    elif(request.method=="POST"):
        form=forms.Predmetiforma(request.POST,instance=predmet)
        if(form.is_valid()):
            form.save()
            return redirect("admin1")
    return render(request,"updatePredmet.html",{"form": form})


def updateKorsnika(request,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    if(request.method=="GET"):
        form=forms.Korsinik(instance=korisnik)
    elif(request.method=="POST"):
        form=forms.Korsinik(request.POST,instance=korisnik)
        if(form.is_valid()):
            form.save()
            return redirect("admin1")
    return render(request,"updateKorsnika.html",{"form": form})

def deletePredmet(request,predmet_id):
    predmet=Predmeti.objects.get(pk=predmet_id)
    predmet.delete()
    return redirect("admin1")

def deleteKorisnika(request,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    korisnik.delete()
    return redirect("admin1")


def upisiStudenta(request,predmet_id,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    predmet=Predmeti.objects.get(pk=predmet_id)

    if not UpisnilistStudent.objects.filter(student=korisnik,predmet=predmet).exists():
        UpisnilistStudent.objects.create(student=korisnik,predmet=predmet,status="upisan")

    return redirect("podaciStudent", korisnik_id)


def podaciStudent(request,korisnik_id):
    predmeti = Predmeti.objects.all()
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    upisani_predmeti=Predmeti.objects.filter(upisniliststudent__student=korisnik)
    return render(request,"podaciStudent.html",{"predmeti":predmeti,"student": korisnik ,"upisani_predmeti":upisani_predmeti})



def IspisiStudenta(request,predmet_id,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    predmet=Predmeti.objects.get(pk=predmet_id)
    UpisnilistStudent.objects.get(student=korisnik,predmet=predmet).delete()
    return redirect("podaciStudent",korisnik_id)



def upisiProfesora(request,predmet_id,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    predmet=Predmeti.objects.get(pk=predmet_id)
    if not UpisnilistProfesora.objects.filter(profesor=korisnik,predmet=predmet).exists():
        UpisnilistProfesora.objects.create(profesor=korisnik,predmet=predmet)
    return redirect("podaciProfesora", korisnik_id)


def podaciProfesora(request,korisnik_id):
    predmeti = Predmeti.objects.all()
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    upisani_predmeti=Predmeti.objects.filter(upisnilistprofesora__profesor=korisnik)
    return render(request,"podaciProfesora.html",{"predmeti":predmeti,"profesor": korisnik ,"upisani_predmeti":upisani_predmeti})



def IspisiProfesora(request,predmet_id,korisnik_id):
    korisnik=Korisnik.objects.get(pk=korisnik_id)
    predmet=Predmeti.objects.get(pk=predmet_id)
    UpisnilistProfesora.objects.get(profesor=korisnik,predmet=predmet).delete()
    return redirect("podaciProfesora",korisnik_id)

def pregledPredmeta(request,predmet_id):
    predmet=Predmeti.objects.get(pk=predmet_id)
    studenti=Korisnik.objects.filter(upisniliststudent__predmet=predmet)
    return render(request,"pregledPredmeta.html",{"predmet":predmet,"studenti":studenti})

def Profesor(request,korisnik_id):
    profesor=Korisnik.objects.get(pk=korisnik_id)
    upisani_predmeti=Predmeti.objects.filter(upisnilistprofesora__profesor=profesor)
    return render(request,"Profesor.html",{"profesori": profesor,"upisani_predmeti":upisani_predmeti})

def PregledPredProf(request,predmet_id):
    profesor= Korisnik.objects.get(pk=request.user.id)
    predmet=Predmeti.objects.get(pk=predmet_id)
    studenti=Korisnik.objects.filter(upisniliststudent__predmet=predmet,upisniliststudent__status="upisan")
    studenti_polozili=Korisnik.objects.filter(upisniliststudent__predmet=predmet,upisniliststudent__status="polozio")
    studenti_izgubilipotpis=Korisnik.objects.filter(upisniliststudent__predmet=predmet,upisniliststudent__status="izgp")
    return render(request,"PregledPredProf.html",{"profesor": profesor, "predmet":predmet,"studenti":studenti,"studenti_polozili":studenti_polozili,"studenti_izgubilipotpis":studenti_izgubilipotpis})


def promejnistatus(request,student_id,predmet_id,status):
    predmet=Predmeti.objects.get(pk=predmet_id)
    student=Korisnik.objects.get(pk=student_id)
    upisniList=UpisnilistStudent.objects.get(student=student,predmet=predmet)
    upisniList.status=status
    upisniList.save()
    return redirect("PregledPredProf",predmet_id)

def Student(request,korisnik_id):
    predmeti = Predmeti.objects.all()
    student=Korisnik.objects.get(pk=korisnik_id)
    upisani_predmeti=Predmeti.objects.filter(upisniliststudent__student=student)
    return render(request,"Student.html",{"student": student,"upisani_predmeti":upisani_predmeti,"predmeti": predmeti})