# Generated by Django 4.2.1 on 2023-06-05 14:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_rename_etc_predmeti_ects'),
    ]

    operations = [
        migrations.CreateModel(
            name='UpisnilistStudent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('upisan', 'upisan'), ('polozio', 'polozio'), ('izgp', 'izgubio pravo potpisa')], default='', max_length=50)),
                ('predmet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.predmeti')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('student', 'predmet')},
            },
        ),
    ]
