from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class Korisnik(AbstractUser):
    ROLES=(("prof","profesor"),("stu","student"),("admin","admin"))
    STATUS=(("none","none"),("izv","izvanredni student"),("red","redovni student"))
    role=models.CharField(max_length=50,choices=ROLES,default="stu")
    status=models.CharField(max_length=50,choices=STATUS,default="none")
    def fullname(self):
        return "%s %s" % (self.first_name,self.last_name)


class Predmeti(models.Model):
    IZBORNI=(("da","da"),("ne","ne"))
    name=models.CharField(max_length=50)
    kod=models.CharField(max_length=50)
    program=models.CharField(max_length=50)
    ects=models.IntegerField()
    sem_red=models.IntegerField()
    sem_izv=models.IntegerField()
    izborni=models.CharField(max_length=50,choices=IZBORNI)

class UpisnilistStudent(models.Model):
    STATUS_UPISNILIST=(("upisan","upisan"),("polozio","polozio"),("izgp","izgubio pravo potpisa"))
    status=models.CharField(max_length=50,choices=STATUS_UPISNILIST,default="")
    student=models.ForeignKey(Korisnik,on_delete=models.CASCADE)
    predmet=models.ForeignKey(Predmeti,on_delete=models.CASCADE)
    class Meta:
        unique_together=("student","predmet")

class UpisnilistProfesora(models.Model):
    profesor=models.ForeignKey(Korisnik,on_delete=models.CASCADE)
    predmet=models.ForeignKey(Predmeti,on_delete=models.CASCADE)
    class Meta:
        unique_together=("profesor","predmet")
