"""
URL configuration for projektIG project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView,LogoutView
from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(template_name="login.html"),name="login"),
    path('logout/', LogoutView.as_view(),name="logout"),
    path('presumjeri/',views.preusmjeri,name="preusmjeri"),
    path('admin1/',views.admin,name="admin1"),
    path("addpredmet/",views.addpredmet,name="addpred"),
    path("addKorsinika/",views.addKorsinika,name="addKorsinika"),
    path("updatePredmet/<int:predmet_id>",views.updatePredmet,name="updatePredmet"),
    path("updateKorsnik/<int:korisnik_id>",views.updateKorsnika,name="updateKorsnika"),
    path("brisiPredmet/<int:predmet_id>",views.deletePredmet,name="deletePredmet"),
    path("deleteKorisnika/<int:korisnik_id>",views.deleteKorisnika,name="deleteKorisnika"),
    path("podaciStudent/<int:korisnik_id>",views.podaciStudent,name="podaciStudent"),
    path("upisiStudenta/<int:predmet_id>/<int:korisnik_id>",views.upisiStudenta,name="upisiStudenta"),
    path("IspisiStudenta/<int:predmet_id>/<int:korisnik_id>",views.IspisiStudenta,name="IspisiStudenta"),
    path("podaciProfesora/<int:korisnik_id>",views.podaciProfesora,name="podaciProfesora"),
    path("upisiProfesora/<int:predmet_id>/<int:korisnik_id>",views.upisiProfesora,name="upisiProfesora"),
    path("IspisiProfesora/<int:predmet_id>/<int:korisnik_id>",views.IspisiProfesora,name="IspisiProfesora"),
    path("pregledPredmeta/<int:predmet_id>",views.pregledPredmeta,name="pregledPredmeta"),
    path("Profesor/<int:korisnik_id>",views.Profesor,name="Profesor"),
    path("PregledPredProf/<int:predmet_id>",views.PregledPredProf,name="PregledPredProf"),
    path("promejnistatus/<int:student_id>/<int:predmet_id>/<str:status>",views.promejnistatus,name="promejnistatus"),
    path("Student/<int:korisnik_id>",views.Student,name="Student"),

]
